//var stringSimilarity = require('string-similarity');
//var chardet = require('chardet');
//var similarity = stringSimilarity.compareTwoStrings('healed', 'sealed'); 
const fs = require('fs');
const stringComparision = require('string-comparision');
const cos = stringComparision.consine;
const chardet = require('chardet');


let k = async function(str1, str2) {
    let charseta = chardet.detect(Buffer.from(str1));
    let charsetb = chardet.detect(Buffer.from(str2));
    let ret = {
        info: [
            charseta, charsetb
        ],
        similarity: cos.similarity(str1, str2),
        distance: cos.distance(str1, str2),
        sortMatch: cos.sortMatch(str1, str2)
    };
    return ret;

}

let y = async function() {
   let a = fs.readFileSync("a.txt", "utf-8");
   let b = fs.readFileSync("b.txt", "utf-8");
   let yy = await k(a, b);
   console.log(yy);
}